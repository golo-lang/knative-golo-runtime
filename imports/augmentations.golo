module knative.golo.rt.augmentations

import gololang.JSON

augment io.vertx.ext.web.Router {
  function get = |this, uri, handler| {
    return this: get(uri): handler(handler)
  }

  function post = |this, uri, handler| {
    return this: post(uri): handler(handler)
  }
}

augment io.vertx.core.http.HttpServerResponse {
  function json = |this, content| ->
    this: putHeader("content-type", "application/json;charset=UTF-8")
        : end(JSON.stringify(content))
}

augment io.vertx.ext.web.RoutingContext {

  function param = |this, param| -> this: request(): getParam(param)

  function bodyAsJson = |this| ->
    JSON.parse(this: getBodyAsString())

  function json = |this, content| {
    this: response(): json(content)
  }
  function 😡 = |this, content| {
    this: response(): json(
      DynamicObject(): error(content)
    )
  }
  function 😃 = |this, content| {
    this: response(): json(
      DynamicObject(): result(content)
    )
  }
}


#augment io.vertx.ext.web.RoutingContext {
#  function bodyAsJson = |this| ->
#    JSON.parse(this: getBodyAsString())

#  function dbody = |this| ->
#    JSON.toDynamicObjectTreeFromString(this: getBodyAsString())
#}