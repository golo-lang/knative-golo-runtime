FROM jponge/golo-lang:latest
WORKDIR /home/app
COPY . .
# --- run the application
CMD golo golo --classpath libs/*.jar --files imports/*.golo main.golo
