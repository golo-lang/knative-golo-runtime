# Knative-Golo-Runtime

## How to use it

### Requirements:

- You need a K8S (or K3S) cluster with Knative installed
- You need to install locally **kubectl** and **kn** (the Knative CLI)
- You need to set `KUBECONFIG`

### Create a Golo service

```bash
service="hello-golo"
namespace="demo"

read -d '' CODE << EOF
function goloFunc = |args| {
  # args' type is io.vertx.core.json.JsonObject
  return ["foo","bar", args: getString("name")]
}
EOF

# create or update the service
kn service create --force ${service} \
  --namespace ${namespace} \
  --env FUNCTION_CODE="${CODE}" \
  --image registry.gitlab.com/golo-lang/knative-golo-runtime:latest
```

### Call Golo service

- get the list of services with URL: `kn route list -n demo` *(where `demo` is the namespace of the service)*

```bash
service="hello-golo"
namespace="demo"

# with httpie
http POST http://${service}.${namespace}.${ingress_base_domain} 'name=👋Bob Morane😃'

# or with curl
curl -d '{"name":"👋Bob Morane😃"}' \
  -H "Content-Type: application/json" \
  -X POST http://${service}.${namespace}.${ingress_base_domain}
```

You'll get:
```json
{
    "result": [
        "foo",
        "bar",
        "👋Bob Morane😃"
    ]
}
```

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md)
